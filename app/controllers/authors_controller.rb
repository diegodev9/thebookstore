class AuthorsController < ApplicationController
  before_action :set_author, only: [:show, :edit, :update, :destroy]

  def index
    @authors = Author.all
  end

  def new
    @page_title = 'Add New Author'
    @author = Author.new
  end

  def show
  end

  def edit
  end

  def create
    @author = Author.new(author_params)
    if @author.save
      redirect_to authors_path, notice: 'Author created'
    else
      render 'new'
    end
  end

  def update
    @author.update(author_params)
    redirect_to authors_path, notice: 'Author updated'
  end

  def destroy
    @author.destroy
    redirect_to authors_path, notice: 'Author removed'
  end

  private

  def set_author
    @author = Author.find(params[:id])
  end

  def author_params
    params.require(:author).permit(:first_name, :last_name)
  end
end
