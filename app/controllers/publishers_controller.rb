class PublishersController < ApplicationController
  before_action :set_publisher, only: [:show, :edit, :update, :destroy]

  def index
    @publishers = Publisher.all
  end

  def new
    @page_title = 'Add New Publisher'
    @publisher = Publisher.new
  end

  def show
  end

  def edit
  end

  def create
    @publisher = Publisher.new(publisher_params)
    if @publisher.save
      redirect_to publishers_path, notice: 'Publisher created'
    else
      render 'new'
    end
  end

  def update
    @publisher.update(publisher_params)
    redirect_to publishers_path, notice: 'Publisher updated'
  end

  def destroy
    @publisher.destroy
    redirect_to publishers_path, notice: 'Publisher removed'
  end

  private

  def set_publisher
    @publisher = Publisher.find(params[:id])
  end

  def publisher_params
    params.require(:publisher).permit(:name)
  end
end
