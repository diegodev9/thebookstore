class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]

  def index
    @categories = Category.all
  end

  def new
    @page_title = 'Add New Category'
    @category = Category.new
  end

  def show
    @categories = Category.all
    @books = @category.books
  end

  def edit
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to categories_path, notice: 'Category created'
    else
      render 'new'
    end
  end

  def update
    @category.update(category_params)
    redirect_to categories_path, notice: 'Category updated'
  end

  def destroy
    @category.destroy
    redirect_to categories_path, notice: 'Category removed'
  end

  private

  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name)
  end
end
