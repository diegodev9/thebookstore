class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]

  def index
    @books = Book.all
    @categories = Category.all
  end

  def new
    @page_title = 'Add Book'
    @book = Book.new
    @category = Category.new
    @author = Author.new
    @publisher = Publisher.new
  end

  def show
    @categories = Category.all
  end

  def edit
  end

  def create
    @book= Book.new(book_params)
    if @book.save
      redirect_to books_path, notice: 'Book added'
    else
      render 'new'
    end
  end

  def update
    @book.update(book_params)
    redirect_to books_path, notice: 'Book updated'
  end

  def destroy
    @book.destroy
    redirect_to books_path, notice: 'Book removed'
  end

  private

  def set_book
    @book = Book.find(params[:id])
  end

  def book_params
    params.require(:book).permit(
      :title,
      :category_id,
      :author_id,
      :publisher_id,
      :isbn,
      :price,
      :buy,
      :format,
      :excerpt,
      :pages,
      :year,
      :coverpath
    )
  end
end
